import numpy as np

def is_squat_deep(poses_df, tol=5):
    """Determines whether squat position broke parallel.
    Returns Boolean

    Input:
    ------
    poses_df -- dataframe with keypoints
    tol -- position tolerance
    """
    y_hip = poses_df[poses_df['body_part'] == 'MidHip'].y_filt
    y_rknee = poses_df[poses_df['body_part'] == 'RKnee'].y_filt
    y_lknee = poses_df[poses_df['body_part'] == 'LKnee'].y_filt
    y_knee = (y_rknee.values+y_lknee.values)/2 # take average for (probably) a more robust est.
    if np.any(y_hip.values < (y_knee + tol)): # for now, just see if it broke parallel at all
        return True
    else: return False

def get_midfoot_pos(poses_df, foot='both'):
    """Determines where midfoot is placed. Takes average
    across time.
    Returns position x,y (unnormalized)

    Input:
    ------
    poses_df -- dataframe with keypoints
    foot -- whether right or left or both (avg)
    """

    x_rbig_toe = poses_df[poses_df['body_part'] == 'RBigToe'].x_filt.values
    x_lbig_toe = poses_df[poses_df['body_part'] == 'LBigToe'].x_filt.values
    x_rheel = poses_df[poses_df['body_part'] == 'RHeel'].x_filt.values
    x_lheel = poses_df[poses_df['body_part'] == 'LHeel'].x_filt.values
    if foot is 'both':
        mid_point_x = ((x_rbig_toe+x_rheel)/2 + (x_lbig_toe+x_lheel)/2)/2
    elif foot is 'right':
        mid_point_x = (x_rbig_toe+x_rheel)/2 
    elif foot is 'left':
        mid_point_x = (x_lbig_toe+x_lheel)/2 
        x_rbig_toe = poses_df[poses_df['body_part'] == 'RBigToe'].x_filt.values

    y_rbig_toe = poses_df[poses_df['body_part'] == 'RBigToe'].y_filt.values
    y_lbig_toe = poses_df[poses_df['body_part'] == 'LBigToe'].y_filt.values
    y_rheel = poses_df[poses_df['body_part'] == 'RHeel'].y_filt.values
    y_lheel = poses_df[poses_df['body_part'] == 'LHeel'].y_filt.values
    if foot is 'both':
        mid_point_y = ((y_rbig_toe+y_rheel)/2 + (y_lbig_toe+y_lheel)/2)/2
    elif foot is 'right':
        mid_point_y = (y_rbig_toe+y_rheel)/2 
    elif foot is 'left':
        mid_point_y = (y_lbig_toe+y_lheel)/2 
    return (np.mean(mid_point_x), np.mean(mid_point_y))