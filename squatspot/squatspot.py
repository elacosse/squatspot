import numpy as np
import pandas as pd
import sys
import os
from sys import platform
import argparse
import json
from scipy import signal
import os
import sys
import shutil
import cv2
import subprocess

import pose_helper as ph
import pose_expert as pe
import pose_action_detection as pad

class SquatSpot:  
    # A class to store kinematics of user
    def __init__(self, save_dir, user_id=None):
        self.OPENPOSE_RUN_COMMAND = "bash call_openpose.sh {PATH}"

        self.uploaded_video_status = False # status variable for whether video was uploaded
        self.user_id = user_id # unique identifier
        self.save_dir = save_dir # save all processed data here
        self.jsons_dir = os.path.join(save_dir, 'jsons')
        if user_id is not None:
            try:
                if not os.path.exists(save_dir):
                    os.makedirs(self.jsons_dir)
            except Exception as e:
                print (e)
        self.input_video_path = None
        self.pose_video_path = None
        self.video_frames = None
        self.num_video_frames = 0
        self.poses_df = None
        self.action_class = None # set to int
        self.expert_feedback = {}


    def init_precomputed_video(self):
        """Initialize from precomputed video"""
        self.input_video_path = os.path.join(self.save_dir, 'input_video.mp4')
        self.video_frames = self.load_video_frames()
        self.num_video_frames = len(self.video_frames)
        self.pose_video_path = os.path.join(self.save_dir, 'pose_video.mp4')
        self.set_and_preproc_openpose_df()
        return 0

    def set_and_preproc_openpose_df(self):
        """Loads OpenPose 2d keypoints json output from body-25 model (POSE_MODEL)"""
        # Load into dataframe
        self.poses_df = ph.load_op_into_df(self.jsons_dir)
        # Preprocess and set
        self.poses_df = ph.preprocess_poses(self.poses_df)
        return 0


    def upload_video(self, video_file_path):
        """Uploads a given video from tmp file save path and load into buffer"""
        save_path = os.path.join(self.save_dir, 'input_video.mp4')
        video_file = open(video_file_path, 'rb')
        try:
            with open(save_path, 'wb') as f:
                shutil.copyfileobj(video_file, f, length=131072)
                video_file.close()
                self.input_video_path = save_path
            self.video_frames = self.load_video_frames()
            self.num_video_frames = len(self.video_frames)
        except Exception as e:
            print(e)
        return 0

    def load_video_frames(self):
        """Loads video frames into numpy array"""
        cap = cv2.VideoCapture(self.input_video_path)
        frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        frameWidth = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        frameHeight = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        buf = np.empty((frameCount, frameHeight, frameWidth, 3), np.dtype('uint8'))
        fc = 0
        ret = True
        while (fc < frameCount  and ret):
            if type(fc) is int:
                try:
                    ret, buf[fc] = cap.read()
                except Exception as e:
                    pass
            fc += 1
        cap.release()
        return buf

    def run_openpose_estimate(self):
        """Runs openpose on command call"""
        command = self.OPENPOSE_RUN_COMMAND.format(PATH=self.save_dir).split()
        process = subprocess.Popen(command, stdout=subprocess.PIPE)
        self.pose_video_path = os.path.join(self.save_dir, 'pose_video.mp4')
        #output, error = process.communicate()
        return 0

    def predict_and_set_action_class(self):
        """Identify exercise class"""
        Xpose = ph.load_op_as_nparray(self.jsons_dir)
        self.action_class = pad.predict_action_class(Xpose)
        return 0

    def generate_and_set_feedback(self):
        """Gives feedback based on lift"""
        ######### Side Back Squat #########
        if self.action_class == 1:
            # Check if it breaks parallel
            self.expert_feedback = {"Reps" : 0, "Good" : "", "Bad" : ""}
            if pe.is_squat_deep(self.poses_df):
                self.expert_feedback["Good"] = ["Your squat reached full depth!"]
            else:
                self.expert_feedback["Bad"] = ["Your squat did not reach full depth."]
        
        else:
            self.expert_feedback = {"Reps" : 0, "Good" : ["NOT IMPLEMENTED"], "Bad" : ["NOT IMPLEMENTED"]}





