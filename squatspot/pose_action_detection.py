import torch
import torch.nn as nn
import numpy as np

PATH_TO_PT_MODEL='../torch/action_recog_v1.pt'

def predict_action_class(X):
    """Predict class that the pose frames belong to
    
    Input:
    ------
    numpy array : 75 dim
    
    Returns:
    --------
    action_class : int marking action class
    """
    Xpose = torch.from_numpy(X).float()
    #############
    class DNN(nn.Module):
        def __init__(self):
            super(DNN, self).__init__()
            self.layers = nn.Sequential(
                nn.BatchNorm1d(75),
                nn.Linear(75, 512),
                nn.ReLU(),
                nn.BatchNorm1d(512),
                nn.Dropout(0.2),
                nn.Linear(512, 75),
                nn.ReLU(),
                nn.BatchNorm1d(75),
                nn.ReLU(),
                nn.Dropout(0.2),
                nn.Linear(75, 6)
            )
        def forward(self, x):
            # convert tensor (128, 1, 28, 28) --> (128, 1*28*28)
            x = x.view(x.size(0), -1)
            x = self.layers(x)
            return x
    ##############
    
    model = DNN()
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    checkpoint = torch.load(PATH_TO_PT_MODEL)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']
    model.eval()

    outputs = model(Xpose)
    pred = outputs.max(1, keepdim=True)[1]
    counts = np.bincount(np.concatenate(pred.tolist())) # find most common class
    class_pred = int(np.argmax(counts))
    return class_pred
    
        