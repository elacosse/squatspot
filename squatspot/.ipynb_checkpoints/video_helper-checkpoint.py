import cv2
import numpy as np

from skimage import data, color
from skimage.transform import hough_circle, hough_circle_peaks, resize
from skimage.feature import canny
from skimage.draw import circle_perimeter, circle, line_aa
from skimage.util import img_as_ubyte

def detect_circle(min_radius, max_radius, edge_image, n_peaks=1, n_radii=1): 
    # Detect radii
    hough_radii = np.arange(min_radius, max_radius, n_radii)
    hough_res = hough_circle(edge_image, hough_radii)
    # Select THE most prominent circle
    return hough_circle_peaks(hough_res, hough_radii,
                                            total_num_peaks=n_peaks)

def get_frames_from_video(filepath):
    """Get numpy frames from video.
    filepath -- path to video
    returns: image frames from video
    """
    cap = cv2.VideoCapture(filepath)
    frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frameWidth = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    buf = np.empty((frameCount, frameHeight, frameWidth, 3), np.dtype('uint8'))
    fc = 0
    ret = True
    while (fc < frameCount  and ret):
        print(fc, buf.shape)
        if type(fc) is int:
            try:
                ret, buf[fc] = cap.read()
            except Exception as e:
                pass
        fc += 1
    cap.release()
    return buf
    
def get_barbell_center(image):
    image = buf[i]
    image_gray = color.rgb2gray(image)
    edges = canny(image_gray, sigma=2.0,
                  low_threshold=0.25, high_threshold=0.4)
    min_radius = np.min(image_gray.shape) // 10
    max_radius = np.min(image_gray.shape) // 3
    _, cx, cy, radii = detect_circle(n_radii=5, min_radius=min_radius, 
                                     max_radius=max_radius, edge_image=edges)
    pos = (cx[0], cy[0])
    return pos