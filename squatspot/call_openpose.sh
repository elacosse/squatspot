
ROOT_SQUATSPOT=/home/ubuntu/squatspot
OPENPOSE_DIR=/home/ubuntu/openpose

#ROOT_SQUATSPOT=/Users/eric/Dropbox/gitlab_repos/squatspot
#OPENPOSE_DIR=/Users/eric/Dropbox/Insight/insight_portfolio_project/ext_repo/openpose

PATH_TO_VIDEO=${ROOT_SQUATSPOT}/data/$1/input_video.mp4
PATH_TO_JSON=${ROOT_SQUATSPOT}/data/$1/jsons
PATH_TO_SAVED_VIDEO=${ROOT_SQUATSPOT}/data/$1/pose_video.mp4
PATH_TO_HEATMAP=${ROOT_SQUATSPOT}/data/$1/heatmaps


cd $OPENPOSE_DIR # needs to run from there!
./build/examples/openpose/openpose.bin --video $PATH_TO_VIDEO --write_json $PATH_TO_JSON --write_video $PATH_TO_SAVED_VIDEO --net_resolution 400x400 --display 0 --render_pose 1
#./build/examples/openpose/openpose.bin --video $1  --write_json $PATH_TO_JSON --heatmaps_add_parts --heatmaps_add_PAFs --write_heatmaps $PATH_TO_HEATMAP --write_video $PATH_TO_SAVED_VIDEO --net_resolution 400x400 --heatmaps_scale 1 --write_heatmaps_format float --display 0 --render_pose 0