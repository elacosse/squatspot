
import numpy as np
import os
import sys
import argparse
def get_heatmap_files_by_time(dirpath):
    # dirpath (str): directory path with float files of pose estimates.
    a = [s for s in os.listdir(dirpath)
         if os.path.isfile(os.path.join(dirpath, s))]
    a.sort(key=lambda s: os.path.getmtime(os.path.join(dirpath, s)))
    a = [f for f in a if f.endswith('.float')]
    return a

def heatmap_prep(filepath, dim2=368, dim3=368):
    """Takes the mean across all heatmaps"""
    # Load custom float format 
    x = np.fromfile(filepath, dtype=np.float32)
    assert x[0] == 3 # First pa
    shape_x = x[1:1+int(x[0])]
    #assert len(shape_x[0]) == 3 # Number of dimensions
    assert shape_x[1] == dim2 # Size of the second dimension
    assert shape_x[2] == dim3 # Size of the third dimension
    array_data = x[1+int(round(x[0])):]
    array_data = array_data.reshape(int(shape_x[0]), int(dim2), int(dim3))
    Xmean = array_data.mean(axis=0)
    Xmedian = np.median(array_data, axis=0)
    return Xmean, Xmedian


# Load heatmaps
#heatmap_path = '/Users/eric/Dropbox/Insight/insight_portfolio_project/squatspot/data/heatmaps/'


parser = argparse.ArgumentParser(description='concatenate heatmaps')
parser.add_argument('path', metavar='N', type=str,
                    help=None)
args = parser.parse_args()
heatmap_path = args.path
filepaths = get_heatmap_files_by_time(heatmap_path)
xmean_concat = []
xmedian_concat = []
for filepath in filepaths:
    Xmean, Xmedian = heatmap_prep(heatmap_path + filepaths)
    xmean_concat.append(np.expand_dims(Xmean, axis=0))
    xmedian_concat.append(np.expand_dims(Xmedian, axis=0))
xmean_concat = np.vstack(xmean_concat)
xmedian_concat = np.vstack(xmedian_concat)

np.save(os.path.join(heatmap_path, 'xmean_concat.npy'), xmean_concat)
np.save(os.path.join(heatmap_path, 'xmedian_concat.npy'), xmedian_concat)