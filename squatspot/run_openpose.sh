
FILES=../train/squat/*
for f in $FILES
do
  echo "Processing $f file..."
  bf="$(basename -- $f)"
  mkdir /data2/elacosse/output/squat/${bf}
  mkdir /data2/elacosse/output/squat/${bf}/heatmap
  ./build/examples/openpose/openpose.bin --video $f  --write_json /data2/elacosse/output/squat/${bf} --heatmaps_add_parts --heatmaps_add_PAFs --write_heatmaps /data2/elacosse/output/squat/${bf}/heatmap --net_resolution 368x368 --heatmaps_scale 1 --write_heatmaps_format float --display 0 --render_pose 0
  python concat_heatmaps.py /data2/elacosse/output/squat/${bf}/heatmap/
  rm -rf /data2/elacosse/output/squat/${bf}/heatmap/*.float 
  #./build/examples/openpose/openpose.bin --video $f --write_video ../data/ --write_keypoint_json ../data/output/poses --no_display
done
FILES=../train/deadlift/*
for f in $FILES
do
  echo "Processing $f file..."
  bf="$(basename -- $f)"
  mkdir /data2/elacosse/output/squat/${bf}
  mkdir /data2/elacosse/output/squat/${bf}/heatmap
  ./build/examples/openpose/openpose.bin --video $f  --write_json /data2/elacosse/output/deadlift/${bf} --heatmaps_add_parts --heatmaps_add_PAFs --write_heatmaps /data2/elacosse/output/deadlift/${bf}/heatmap --net_resolution 368x368 --heatmaps_scale 1 --write_heatmaps_format float --display 0 --render_pose 0
  python concat_heatmaps.py /data2/elacosse/output/deadlift/${bf}/heatmap/
  rm -rf /data2/elacosse/output/deadlift/${bf}/heatmap/*.float 
done





# ./build/examples/openpose/openpose.bin --video small_test.mp4  --write_video ./output/small_test/small_test.mp4 --write_json ./output/small_test --heatmaps_add_parts --heatmaps_add_PAFs --write_heatmaps ./output/small_test/heatmap --net_resolution 368x368 --heatmaps_scale 1 --write_heatmaps_format float --display 0 --render_pose 0




heatMapFullPath='52_000000000000_pose_heatmaps.float'
# Load custom float format - Example in Python, assuming a (18 x 300 x 500) size Array
x = np.fromfile(heatMapFullPath, dtype=np.float32)
assert x[0] == 3 # First parameter saves the number of dimensions (18x300x500 = 3 dimensions)
shape_x = x[1:1+int(x[0])]
#assert len(shape_x[0]) == 3 # Number of dimensions
assert shape_x[0] == 77 # Size of the first dimension
assert shape_x[1] == 368 # Size of the second dimension
assert shape_x[2] == 368 # Size of the third dimension
arrayData = x[1+int(round(x[0])):]

# collapse into one heatmap