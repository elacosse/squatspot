import pandas as pd
import shutil
import time
import os
import subprocess
import numpy as np
import matplotlib.pyplot as plt
import datetime

import squatspot as ss
import pose_expert as pe
import video_helper as vh
import streamlit as st

PATH_TO_OPENPOSE='/home/ubuntu/openpose/'
basedir_datapath = '../data'
openpose_run_command = "bash " + PATH_TO_OPENPOSE + "call_openpose.sh"

# Status variables for Streamlit executions
submit_pose = False
pose_playback = False
run_action_recog = False
engage_expert = False
running_example = False
upload_engaged = False
track_barbell = False
plot_barbell = False

def _max_width_():
    """
    Increase Streamlit app width to fullscreen.

    Fom https://discuss.streamlit.io/t/custom-render-widths/81/8
    """

    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>    
    """,
        unsafe_allow_html=True,
    )
# List indices correspond to class prediction model
lift_choice_types = ['Back Squat - Front', 'Back Squat - Side', 
                     'Back Squat - Angled', 'Deadlift - Front', 
                     'Deadlift - Side', 'Deadlift - Angled']

################ Intro/Title ###########################
st.title("SquatSpot")
st.write(
    """
    Welcome to SquatSpot, a tool for you to get tips on performing back squats 
    for maximal safety and performance. It uses pose estimation to try and figure
    out where your form might be going wrong on those heavy lifts!
    """
)

################ Pre-computed examples in sidebar #################
examples = [None, 'squat1', 'squat2', 'squat3', 'squat4', 
            'squat5', 'squat6', 'deadlift1', 'deadlift2']
st.sidebar.title("Example Selection")
st.sidebar.markdown("Here are some pre-computed inputs")
example_selection = st.sidebar.selectbox(
    'Select a random video already uploaded',
    list(examples), 0)

####### Create SpotSpot object with uploaded video #########
if isinstance(example_selection, str): # if precomputed example
    user_id = example_selection
    squatspot = ss.SquatSpot(os.path.join(basedir_datapath, user_id), user_id)
    squatspot.init_precomputed_video()
    running_example = True
    pose_playback = True
    run_action_recog = True
    upload_engaged = True

elif upload_engaged is False: # self uploaded video
    # Create a random name for new video being uploaded with basename version
    basename = "v1_" # prefix for folder
    suffix = datetime.datetime.now().strftime("%y%m%d_%H%M")
    user_id = basename + suffix
    ss_path = os.path.join(basedir_datapath, user_id)
    # Initialize squatspot obj to path and user id
    squatspot = ss.SquatSpot(ss_path, user_id)
    upload_engaged = True


################ File Upload and playback ##########################
st.header("Upload a video file of yourself performing a set.")
video_file = st.file_uploader(label="Input Video", type=["mp4"], encoding="auto")
if (video_file is not None) and (upload_engaged is True):
    tmp_file = '../data/tmp.mp4'
    try:
        video_file.seek(0)
        with open(tmp_file, 'wb') as f:
            shutil.copyfileobj(video_file, f, length=131072)
            video_file.close()
    except Exception as e:
        print(e)
    # save to tmp_file and then pass in to upload video
    squatspot.upload_video(tmp_file)

if squatspot.num_video_frames > 0:
    st.write('Number of video frames: ' + str(squatspot.num_video_frames))
    video_file = open(squatspot.input_video_path, 'rb')
    video_bytes = video_file.read()
    st.video(video_bytes) # Video playback widget for user
    video_file.close()

################ Process Video into DataFrame ##########################
submit_pose = st.button('Process Video for Pose Estimation')
if submit_pose and not running_example:
    squatspot.run_openpose_estimate()
    #'Starting a long computation by getting pose estimate.
    latest_iteration_1 = st.empty()
    pose_video_progress = st.progress(0)
    # Get number of frame files output to folder.
    while True:
        try: 
            file_list = os.listdir(squatspot.jsons_dir)
            number_files = len(file_list)
            progress_int = int(100*float(number_files)/squatspot.num_video_frames)
            if progress_int >= 0:
                pose_video_progress.progress(progress_int)
                latest_iteration_1.text(f'Processing frame {number_files}')
            if number_files >= squatspot.num_video_frames:
                pose_playback = True
                run_action_recog = True
                break
        except Exception as e:
            raise e
    st.success('Pose estimation done!')

if pose_playback is True:
    # Display pose estimate
    video_file = open(squatspot.pose_video_path, 'rb')
    video_bytes = video_file.read()
    st.video(video_bytes)
    video_file.close()
    # Load pose keypoint outputs and preprocessf
    squatspot.set_and_preproc_openpose_df()
    run_action_recog = True # after pose playback, run action recognition

################## Action Recognition ###########################


#Figure out if squat or deadlift, front or side.
#lift_choice_type_detected_index = 0
if run_action_recog:
    # Prediction action class
    squatspot.predict_and_set_action_class()
    st.write("## *Detected lift:* ")
    #lift_type = st.selectbox("Detected lift: ", lift_choice_types, squatspot.action_class)
    engage_expert = True
    # If reselected because of incorrect prediction, get the actual index! TODO

if engage_expert:
    # Get feedback from squatspot
    st.write('## ' + lift_choice_types[squatspot.action_class] + 
             ' detected - Here is your feedback:')
    squatspot.generate_and_set_feedback()
    for feedback in squatspot.expert_feedback["Good"]:
        st.write('### &#x2611 ' + feedback)
    for feedback in squatspot.expert_feedback["Bad"]:
        st.write('### &#x2612 ' + feedback)
    # if camera is from the SIDE, then have option to track barbell.
    if ((squatspot.action_class == 1) or (squatspot.action_class == 4)):
        track_barbell = True

if track_barbell == True:
    submit_track = st.button('Track barbell kinematics from side view')
    if submit_track and plot_barbell == False:
        # Plot midfoot position and line up to barbell
        mid_foot = pe.get_midfoot_pos(squatspot.poses_df, foot='both')
        latest_iteration_2 = st.empty()
        bb_video_progress = st.progress(0)
        bb_xy = []
        
        subsample_factor = 1
        resize_factor = 2
        # Get video
        #resized_mask = vh.get_resized_mask(squatspot.video_frames, resize_factor, subsample_factor)
        #frames = squatspot.video_frames
        #poses_df = squatspot.poses_df
        if squatspot.video_frames.shape[1] > 600:
            subsample_factor = 1
            resize_factor =  4
        else:
            subsample_factor = 1
            resize_factor = 2

        for i in range(0, squatspot.video_frames.shape[0], subsample_factor):
            image = squatspot.video_frames[i]
            #pos = vh.get_barbell_xy_from_side_vid(image, resized_mask, resize_factor)
            pos, _ = vh.extract_barbell_from_frame_w_pose(squatspot.poses_df,
                                                          image, i, resize_factor)
            if (len(pos[0]) != 0):
                bb_xy.append((pos[0][0], pos[1][0]))
            progress_int = int(100*(i+1)//
                                (squatspot.video_frames.shape[0]/subsample_factor))
            latest_iteration_2.text(f'Processing frame {i+1}')
            bb_video_progress.progress(progress_int)
        bb_video_progress.progress(100)
        st.success('Barbell tracking estimation done!')
        bb_xy = np.array(bb_xy)
        bb_xy, remove_indices = vh.remove_outliers_and_smooth(bb_xy, 1)
        plot_barbell = True

        # Display frames that have outliers removed.
        plot_bb_frames = np.copy(squatspot.video_frames)
        plot_bb_frames = np.delete(plot_bb_frames, remove_indices, axis=0)

        if plot_barbell == True:
            frame_indx = 0
            frame = plot_bb_frames[frame_indx]
            #_max_width_()
            plt.imshow(frame)
            
            plt.plot((mid_foot[0], mid_foot[0]), (mid_foot[1], bb_xy[0][1]),
                     linewidth=4, color='green')
            plt.plot(bb_xy[:,0],bb_xy[:,1], color='red')
            plt.xticks([])
            plt.yticks([])
            plt.tight_layout()
            st.pyplot()


            # Make widget to track barbell TODO
            #frame_indx = st.slider('Select a range of values', 0, plot_bb_frames.shape[0] , 0)


