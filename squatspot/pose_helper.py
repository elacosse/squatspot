"""
Some custom utilities to load, process, save OpenPose data
Requires openpose python API to be installed.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import os
from sys import platform
import argparse
import json
from scipy import signal

###########################################
# OPENPOSE_PYTHON_RELEASE_DIR='/Users/eric/Dropbox/Insight/insight_portfolio_project/ext_repo/openpose/build/python'
# # Import Openpose (Windows/Ubuntu/OSX)
# try:
#     # Change these variables to point to the correct folder (Release/x64 etc.)
#     sys.path.append(OPENPOSE_PYTHON_RELEASE_DIR)
#     # If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
#     # sys.path.append('/usr/local/python')
#     from openpose import pyopenpose as op
# except ImportError as e:
#     print('Error: OpenPose library could not be found. Did you enable `BUILD_PYTHON` in CMake and have this Python script in the right folder?')
#     raise e
# POSE_MODEL=op.PoseModel.BODY_25

# If python module isn't built, here's the BODY 25 model.
body_parts = {0: 'Nose',
 1: 'Neck',
 2: 'RShoulder',
 3: 'RElbow',
 4: 'RWrist',
 5: 'LShoulder',
 6: 'LElbow',
 7: 'LWrist',
 8: 'MidHip',
 9: 'RHip',
 10: 'RKnee',
 11: 'RAnkle',
 12: 'LHip',
 13: 'LKnee',
 14: 'LAnkle',
 15: 'REye',
 16: 'LEye',
 17: 'REar',
 18: 'LEar',
 19: 'LBigToe',
 20: 'LSmallToe',
 21: 'LHeel',
 22: 'RBigToe',
 23: 'RSmallToe',
 24: 'RHeel',
 25: 'Background'}
###########################################

def get_json_files_by_time(dirpath):
    # dirpath (str): directory path with .json files of pose estimates.
    a = [s for s in os.listdir(dirpath)
         if os.path.isfile(os.path.join(dirpath, s))]
    a.sort(key=lambda s: os.path.getmtime(os.path.join(dirpath, s)))
    a = [f for f in a if f.endswith('.json')]
    return a

def read_json(file_path):
    # filepath (str) : read in a json file
    try:
        with open(file_path) as f:
            data = json.load(f)
            return data
    except Exception as e:
        print(e)

def parse_pose(pose_data):
    # Parses pose in parts associated with the pose.
    if pose_data is None:
        return []
    # body_parts = op.getPoseBodyPartMapping(POSE_MODEL)
    poses = []
    try:
        num_people = len(pose_data)
        if num_people >= 1:
            # ToDo
            # run check to eliminate other people a select first
            
            pose_data = pose_data['people'][0] # take first person.
            
            p_kp_data = pose_data['pose_keypoints_2d']
            
            pose_points = np.array(p_kp_data).reshape(25,3)
            
            poses.append(pose_points)
            
    except Exception as e:
        print(e)
    return np.vstack(poses)

def find_trim_section(keypoints_df):
    """Finds beginning and end of set. Returns frames belonging in set"""
    # ToDo
    return 0

def preprocess_poses(df):
    """Smooth signals in time so that pose estimation isn't sojittery.
       1. Apply a Savitzky-Golay filter on all parts.
       2. Normalize.
    Keyword arguments:
    df -- dataframe with pose keypoints estimated"""
    # 1. Temporal smoothing
    WINDOW_LENGTH=17
    POLY_ORDER=3

    df['x_filt'] = 0
    df['y_filt'] = 0
    #for body_part in list(op.getPoseBodyPartMapping(POSE_MODEL).values())[0:-1]:
    for body_part in list(body_parts.values())[0:-1]:
        for coord in ['x', 'y']:
            s = df[df['body_part'] == body_part][coord]
            s = signal.savgol_filter(s, window_length=WINDOW_LENGTH, polyorder=POLY_ORDER)
            df.loc[df['body_part'] == body_part, coord+'_filt'] = s
            
    # 2: normalization
    return df

def load_op_into_df(filepath):
    """Loads OpenPose 2d keypoints json output from body-25 model (POSE_MODEL)
    """
    files = get_json_files_by_time(filepath)
    poses = [] 
    poses_df = []
    for frame, f in enumerate(files):
        d = read_json(os.path.join(filepath, f))
        if len(d['people']) != 0: # make sure there's a person in json.
            pose = parse_pose(d)
            poses.append(pose)
            df = pd.DataFrame(pose)
            df['frame'] = frame
            #df['body_part'] = list(op.getPoseBodyPartMapping(POSE_MODEL).values())[0:-1]
            df['body_part'] = list(body_parts.values())[0:-1]
            poses_df.append(df)
    poses_df = pd.concat(poses_df)
    poses_df.rename(columns = { 0 : 'x', 1 : 'y',  2 : 'confidence'}, inplace = True)

    # Preprocess
    poses_df = preprocess_poses(poses_df)
    return poses_df


def load_op_as_nparray(filepath):
    """Loads OpenPose 2d keypoints as numpy array from body-25 model
    Input:
    ------
    filepath : string, json filepath directory

    Output:
    -------
    
    """
    X = []
    files = get_json_files_by_time(filepath)
    for frame, f in enumerate(files):
        d = read_json(os.path.join(filepath, f))
        if len(d['people']) != 0: # make sure there's a person in json. 
            frame_pose = parse_pose(d)
            x = frame_pose.flatten()
            X.append(x)
    X = np.vstack(X)
    return X