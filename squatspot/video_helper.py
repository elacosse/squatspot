import cv2
import numpy as np
import scipy
from skimage import data, color
from skimage.transform import hough_circle, hough_circle_peaks, resize
from skimage.feature import canny
from skimage.draw import circle_perimeter, circle, line_aa
from skimage.util import img_as_ubyte
import matplotlib.pyplot as plt
#def plot_bb_side(xys, mid_foot_pos)


def detect_circle(min_radius, max_radius, edge_image, n_peaks=1, n_radii=1): 
    # Detect radii
    hough_radii = np.arange(min_radius, max_radius, n_radii)
    hough_res = hough_circle(edge_image, hough_radii)
    # Select THE most prominent circle
    return hough_circle_peaks(hough_res, hough_radii,
                                            total_num_peaks=n_peaks)

def get_frames_from_video(filepath):
    """Get numpy frames from video.
    filepath -- path to video
    returns: image frames from video
    """
    cap = cv2.VideoCapture(filepath)
    frameCount = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frameWidth = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frameHeight = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    buf = np.empty((frameCount, frameHeight, frameWidth, 3), np.dtype('uint8'))
    fc = 0
    ret = True
    while (fc < frameCount  and ret):
        if type(fc) is int:
            try:
                ret, buf[fc] = cap.read()
            except Exception as e:
                pass
        fc += 1
    cap.release()
    return buf


def extract_barbell_from_frame_w_pose(poses_df, frame, frame_indx, resize_factor=2):
    """Return center of barbell from video frame taking into account
    the pose estimate position and other factors that speed up estimation.
    Uses some masking heuristics based on pose estimate location.
    Circle detection is computed by edge detection and Hough transforms.
    Input
    --------
    poses_df : pose estimate of video, pandas DataFrame
    frame : image of video, numpy array
    frame_indx : index to select from poses_df, integer
    resize_factor : resample the image for faster to size down for faster computation, 
    integer = 2

    Output
    -------
    (cx*resize_factor, cy*resize_factor) : tuple with position resize to original
        cx,cy are float x,y image coordinates
    edges_masked : edge image where the hough transform is computed, numpy array.
    """

    # Resize by resize_factor and convert to gray scale
    frame = resize(frame, (frame.shape[0] // resize_factor, 
                           frame.shape[1] // resize_factor),
                   anti_aliasing=True, preserve_range=False)
    gray_frame = color.rgb2gray(frame)
  
    if cv2.countNonZero(gray_frame) != 0:
        # Extract pose estimate at frame
        frame_df = poses_df[poses_df['frame'] == frame_indx]
        # Get mask heuristic from pose estimate at hip and shoulder
        x_shoulder = (frame_df.loc[2,'x_filt']+frame_df.loc[5,'x_filt'])/2
        y_shoulder = (frame_df.loc[2,'y_filt']+frame_df.loc[5,'y_filt'])/2
        x_hip = frame_df.loc[8,'y_filt']
        y_hip = frame_df.loc[8,'y_filt']
        thrx = (y_shoulder-y_hip)
        thry = (y_shoulder-y_hip)*1.5 # extend a little bit

        # Draw a rectangle as mask
        points = np.array([[((x_shoulder-thrx), y_shoulder+thry),
                            ((x_shoulder+thrx), y_shoulder+thry),
                            ((x_shoulder+thrx), y_shoulder-thry),
                            ((x_shoulder-thrx), y_shoulder-thry)]], dtype=np.int32)
        points = points // resize_factor # resize
        mask = np.zeros_like(gray_frame)
        ignore_mask_color = 255 
        poly_mask = cv2.fillPoly(mask, points, ignore_mask_color)
        # frame_masked = gray_frame * (poly_mask > 0)
        
        
        gray_frame = np.uint8(gray_frame*255) # Convert to Uint8 for cv2
        # Apply gaussian smoothing
        gray_frame = cv2.GaussianBlur(gray_frame,(5,5),10)
        sigma = 0.33
        v = np.median(gray_frame)
        # Apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edges = cv2.Canny(gray_frame, lower, upper)
        edges_masked = edges * (poly_mask > 0) # Take only mask area

        min_radius = np.min(gray_frame.shape) // 9
        max_radius = np.min(gray_frame.shape) // 5
        _, cx, cy, radii = detect_circle(n_radii=1, min_radius=min_radius, 
                                      max_radius=max_radius, edge_image=edges_masked)
        # Resize center estimate on return
        return (cx*resize_factor, cy*resize_factor), edges_masked
    else:
        return ([], []), None


# Outlier detection
def remove_outliers_and_smooth(xys,zthr=1):
    """Removes outiers in x direction. Then smoothes center estimates

    Input
    --------
    xys : x,y coordinates, numpy array 2xN
    zthr : z-score threshold to detect ouliers, float
    """

    WINDOW_LENGTH=17
    POLY_ORDER=3
    # Zscore
    x = xys[:,0]
    y = xys[:,1]
    #xz = scipy.stats.zscore(np.diff(x))
    xz = scipy.stats.zscore(x)
    remove_xz_indx = np.where((xz > zthr) | (xz < -zthr))[0]
    remove_indx = remove_xz_indx
    #yz = scipy.stats.zscore(np.diff(y))
    #remove_yz_indx = np.where((yz > zthr) | (yz < -zthr))[0] + 1
    #remove_indx = np.unique(np.concatenate((remove_xz_indx, remove_yz_indx)))
    new_xys = np.delete(xys, remove_indx, axis=0)
    new_xys[:,0] = scipy.signal.savgol_filter(new_xys[:,0], 
        window_length=WINDOW_LENGTH, polyorder=POLY_ORDER)
    new_xys[:,1] = scipy.signal.savgol_filter(new_xys[:,1], 
        window_length=WINDOW_LENGTH, polyorder=POLY_ORDER)
    return new_xys, remove_indx


# Some deprecated functions
def compute_std_mask(vid, std_threshold=0.4):
    std_vid = np.std(vid, axis=0)
    std_vid /= np.max(std_vid)
    vid_bin = (std_vid > std_threshold) * 255
    return vid_bin

def get_resized_mask(rgb_video, subsample_factor=1, resize_factor=2):
    """Extracts gray video from RGB and 
    Input
    ---------
    rgb_video : numpy array of rgb video (t, X, Y, rgb)
    subsample_factor : int, how many frames should be sampled 1 = all, 5 = every 5
    Output
    --------- 
    """
    resize_factor = 2
    std_threshold = 0.25
    gray_video = np.vstack(
        [np.expand_dims(cv2.cvtColor(rgb_video[i], cv2.COLOR_BGR2GRAY), axis=0) 
         for i in range(0, rgb_video.shape[0], subsample_factor)])

    # Compute mask based on temporal variation of video frames
    std_mask = compute_std_mask(gray_video, std_threshold)
    std_mask = resize(std_mask, (rgb_video.shape[1] // resize_factor, 
                                 rgb_video.shape[2] // resize_factor),
                      anti_aliasing=True)
    # Erosion and dilation operators
    kernel = np.ones((5,5),np.int8)
    erosion = cv2.erode(std_mask, kernel, iterations = 4)
    erosion_dilation = cv2.dilate(erosion, kernel, iterations = 8)
    mask = erosion_dilation
    return mask

def get_barbell_xy_from_side_vid(img_rgb, std_mask, resize_factor=2):
    """Extracts barbell center coordinates from video frames
    
    Input
    ---------
    rgb_video : numpy array of rgb video (t, X, Y, rgb)
    subsample_factor : int, how many frames should be sampled 1 = all, 5 = every 5
    
    Output
    ---------
    cx, cy : imagbarbell centers
    """

    # Load picture, convert to grayscale and detect edges
    img_resized = resize(img_rgb, (img_rgb.shape[0] // resize_factor, 
                                    img_rgb.shape[1] // resize_factor),
                            anti_aliasing=True)
    img_gray = color.rgb2gray(img_resized)

    edges = canny(img_gray, sigma=2.0,
                low_threshold=0.1, high_threshold=0.2)
    edges = edges * std_mask # Take edges only within mask

    min_radius = np.min(img_gray.shape) // 8
    max_radius = np.min(img_gray.shape) // 2

    _, cx, cy, radii = detect_circle(n_radii=10, min_radius=min_radius, 
                                    max_radius=max_radius, edge_image=edges)
    return cx*resize_factor, cy*resize_factor
