******************************************
SquatSpot : Check yourself before you wreck yourself!
******************************************

Introduction
============

:Author: Eric Lacosse
:Web site: http://gitlab.com/elacosse/squatspot
:Version: 0.0.1
:License: SquatSpot is released under the GNU General Public License v3 (http://www.gnu.org/licenses/gpl-3.0.txt)

Purpose
=======

SquatSpot was an Insight Data Science Fellow project I quickly put together
to give weightlifters the ability to check some aspects of their form. It incorporates
pose estimation, action recognition, and form heuristics together. This 
project intended to investigate how feasible such a system would be and where
important bottlenecks in computer vision would be.

The project ran on an AWS EC2 for backend computations. All
frontend aspects are handled by streamlit.

Development status
==================
This is alpha code.


Installation
============

Dependencies
------------

* Python 3.6+
* Numpy
* Scipy
* Skimage
* Pandas
* Streamlit
* OpenCV
* Pytorch
* A working build of OpenPose running the 25-Bodypart model (https://github.com/CMU-Perceptual-Computing-Lab/openpose) 



Acknowledgements
================

SquatSpot uses OpenPose for pose estimation.

@inproceedings{cao2018openpose,
  author = {Zhe Cao and Gines Hidalgo and Tomas Simon and Shih-En Wei and Yaser Sheikh},
  booktitle = {arXiv preprint arXiv:1812.08008},
  title = {Open{P}ose: realtime multi-person 2{D} pose estimation using {P}art {A}ffinity {F}ields},
  year = {2018}
}